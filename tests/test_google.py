from selenium.webdriver.common.by import By

def test_query_window_is_visible(remote_browser):
   remote_browser.get('https://google.com')
   query_window = remote_browser.find_element_by_name('q')
   assert query_window.is_displayed()


def test_first(remote_browser):
   remote_browser.get("https://demo.opencart.com/admin/") # https://demo.opencart.com  f"{remote_browser.base_url}/admin/
   remote_browser.find_element(By.CSS_SELECTOR, "#input-username").clear()
   remote_browser.find_element(By.CSS_SELECTOR, "#input-username").send_keys("demo")
   remote_browser.find_element(By.CSS_SELECTOR, "#input-password").clear()
   remote_browser.find_element(By.CSS_SELECTOR, "#input-password").send_keys("demo")
   remote_browser.find_element(By.CSS_SELECTOR, "button[type='submit']").click()
   remote_browser.find_element(By.CSS_SELECTOR, "#user-profile")


def test_admin_reset_password(remote_browser):
   remote_browser.get("https://demo.opencart.com/admin/")
   remote_browser.find_element(By.LINK_TEXT, "Forgotten Password").click()
   remote_browser.find_element(By.CSS_SELECTOR, "#input-email").clear()
   remote_browser.find_element(By.CSS_SELECTOR, "#input-email").send_keys("not_existing_mail@mail.ru")
   remote_browser.find_element(By.CSS_SELECTOR, "button[type='submit']").click()
   remote_browser.find_element(By.CSS_SELECTOR, ".alert-danger")

# pytest --browser chrome // прмиер для запуска тестов


