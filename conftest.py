import pytest
from selenium import webdriver
from selenium.webdriver import Remote
from selenium.webdriver.chrome.options import Options


def pytest_addoption(parser):
    """ Parse pytest --option variables from shell """
    parser.addoption('--browser',
                     help='Which test browser?',
                     default='chrome')
    parser.addoption('--url', default='https://demo.opencart.com')


@pytest.fixture(scope='session')
def test_browser(request):
    return request.config.getoption('--browser')


@pytest.fixture(scope='function')
def remote_browser(test_browser) -> Remote:
    """ Select configuration depends on browser """

    # ДЛЯ  ЛОКАЛЬНОГО ЗАПУСКА В ХРОМЕ:
    # driver = webdriver.Chrome(
    #     executable_path="C:\\Users\\11\\Downloads\\python-gitlabci-selenium-2.0.0\\python-gitlabci-selenium-2.0.0\\tests\\chromedriver",
    #     options=Options())
    # yield driver
    # driver.quit()  # Закрываем браузер

    # ДЛЯ ЗАПУСКА В ci:
    if test_browser == 'firefox':
        driver = webdriver.Remote(
            options=webdriver.FirefoxOptions(),
            command_executor='http://selenium__standalone-firefox:4444/wd/hub'
        )
        yield driver
        driver.quit()  # Закрываем браузер
        # return driver

    elif test_browser == 'chrome':
        driver = webdriver.Remote(
            options=webdriver.ChromeOptions(),
            command_executor='http://selenium__standalone-chrome:4444/wd/hub'
        )
        yield driver
        driver.quit()  # Закрываем браузер
        # return driver

    elif test_browser == 'edge':
        driver = webdriver.Remote(
            command_executor='http://selenium__standalone-edge:4444/wd/hub'
        )
        yield driver
        driver.quit()  # Закрываем браузер
        # return driver

    # elif test_browser == 'opera':
    #     driver = webdriver.Remote(
    #         options=webdriver.OperaOptions(),
    #         command_executor='http://selenium__standalone-opera:4444/wd/hub'
    #     )
    ##   base_url = test_browser.config.getoption('--url')
    ##   driver.base_url = base_url
    #     return driver

    else:
        raise ValueError(f'--browser={test_browser} is not chrome or firefox')

# pytest -v --browser chrome // для запуска
# pytest -v --browser chrome  --html=report.html --self-contained-html "./tests" // для запуска с генерацией отчета
